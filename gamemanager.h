#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H
#include "player.h"

class gameManager
{
public:
    Player player;
    void displayMenu();
    void choiceMenu();
    void playGame();
    void init();
    void playerBoard();
    gameManager();

};

#endif // GAMEMANAGER_H
