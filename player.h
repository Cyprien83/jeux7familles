#ifndef PLAYER_H
#define PLAYER_H
#include <card.h>
#include <iostream>


class Player
{
public:
    std::string pseudo;
    std::string lastname;
    std::string firstname;
    std::vector<Card>  OwnedCard[7];
    int victory;
    int familyMakedInGame;
    bool realPlayer;
    bool askForACard(Card cardAsked, Player playerAsk);
    void giveACard(Card CardAsked, Player playerAsking);
    void setPlayer(std::string lastname, std::string firstname, std::string pseudo, int victory);
    void createPlayer(Player player);
    bool loadPlayer();
    void checkFamily();
    Player(bool real, std::string pseudoBot);
    Player(bool real);
    Player();
};

#endif // PLAYER_H
