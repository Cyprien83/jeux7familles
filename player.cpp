#include "player.h"
#include "card.h"
#include <QFile>
#include <iostream>
#include <QTextStream>

//char pseudo;
//Card ownedCard[7];
//int gameWin;
//int familyMakedInGame;
//bool realPlayer;

using namespace std;

Player::Player()
{

}
Player::Player(bool real, string pseudo){

    this->realPlayer = real;
    this->pseudo = pseudo;
}
Player::Player(bool real){
    this->realPlayer = real;
}
bool Player::askForACard(Card cardAsked, Player playerAsk)
{

}

void Player::giveACard(Card CardAsked, Player playerAsking)
{

}

void Player::createPlayer(Player existingPlayer)
{
    if (loadPlayer()) {
        string response;
        cout << "Un profil de " << existingPlayer.pseudo << " deja existant voulez-vous en creer un autre ? (y/n)" << endl;
        cin >> response;
        if (response != "y") {
            return;
        }
    }
    string pseudo;
    cout << "Entrer un pseudo :";
    cin >> pseudo;

    string firstName;
    cout << "Entrer un prenom :";
    cin >> firstName;

    string lastName;
    cout << "Entrer un nom :";
    cin >> lastName;

    QFile player("./player.txt");
    if (player.exists()) {
        if(player.open(QIODevice::WriteOnly)) {
            QTextStream stream(&player);
            stream << QString::fromStdString(pseudo) << endl
                   << QString::fromStdString(firstName) << endl
                   << QString::fromStdString(lastName) << endl
                   << QString::fromStdString("0");
        }
    } else {
        player.open(QIODevice::WriteOnly);
        QFile player("./player.txt");
        if(player.open(QIODevice::WriteOnly)) {
            QTextStream stream(&player);
            stream << QString::fromStdString(pseudo) << endl
                   << QString::fromStdString(firstName) << endl
                   << QString::fromStdString(lastName) << endl
                   << QString::fromStdString("0");
        }
    }
    setPlayer(lastName, firstName, pseudo, victory);
    cout << "Profil cree !" << endl;
    cout << "" << endl;
}

bool Player::loadPlayer()
{
    QFile player("./player.txt");
    if(player.exists()){
        player.open(QIODevice::ReadOnly | QIODevice::Text);
        QTextStream stream(&player);
        QString pseudo = stream.readLine();
        QString firstname = stream.readLine();
        QString lastname = stream.readLine();
        int victory = stream.readLine().toInt();
        setPlayer(lastname.toStdString(), firstname.toStdString(), pseudo.toStdString(), victory);
        player.close();
    }
    return player.exists();
}

void Player::setPlayer(string lastname, string firstname, string pseudo, int victory) {
    this->realPlayer = true;
    this->lastname = lastname;
    this->firstname = firstname;
    this->pseudo = pseudo;
    this->victory = victory;
}

void Player::checkFamily()
{

}
