#include "gamemanager.h"
#include <iostream>
#include "player.h"
#include "game.h"

using namespace std;

gameManager::gameManager()
{
    init();
    displayMenu();
    choiceMenu();
}

void gameManager::init()
{
    Player player = Player(true);
    this->player = player;
    //    bool playerExist = player.loadPlayer();

    if (!this->player.loadPlayer()) {
        cout << "Bienvenue !" << endl;
        cout << "Veuiller creer un profil" << endl;
        this->player.createPlayer(this->player);
    }
}

void gameManager::displayMenu()
{
    cout << "****************" << endl;
    cout << "***** MENU *****" << endl;
    cout << "****************" << endl;
    cout << "" << endl;
    cout << "1 - Creation d'un profil" << endl;
    cout << "2 - Jouer" << endl;
    cout << "3 - Profil" <<endl;

    choiceMenu();

}

void gameManager::choiceMenu()
{
    int response;
    cout << "=>";
    cin >> response;

    Game game;

    switch (response) {
    case 1:
        this->player.createPlayer(this->player);
        displayMenu();
        break;
    case 2:
        game.RunGame(this->player);
        displayMenu();
        break;
    case 3:
        playerBoard();
        displayMenu();
        break;
    default:
        cout << "Entree invalide (1,2 ou 3)" << endl;
        displayMenu();
        break;
    }
}

void gameManager::playGame()
{
    cout << "" << endl;
    cout << "Selection du joueur :" << endl;
}

void gameManager::playerBoard()
{
    cout << "" << endl;
    cout << "*****Informations*****" << endl;
    cout << "Pseudo : " << this->player.pseudo << endl;
    cout << "Nom : " << this->player.lastname << endl;
    cout << "Prenom : " << this->player.firstname << endl;
    cout << "Victoire : " << this->player.victory << endl;
    cout << "" << endl;

    return;
}
