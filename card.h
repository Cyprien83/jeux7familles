#ifndef CARD_H
#define CARD_H
#include <QString>

class Card
{
public:
    Card();
    Card(QString cardType, QString family);
    QString cardtype;
    QString family;

};

#endif // CARD_H
