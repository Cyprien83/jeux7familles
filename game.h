#ifndef GAME_H
#define GAME_H
#include<card.h>
#include<player.h>
#include <QString>
#include <array>
#include <iterator>
#include <vector>

class Game
{
public:
    bool GameWin = false;
    std::vector<Card>  pick[42];
    Player playerTurn;
    std::vector<Player>  playerArray[4];

    void sendGameStat(Player winnerPlayer);
    void nextPlayer();
    void ShuffleDeck();
    void SetPlayer(Player play1);
    void GiveCardPlayer();
    void RunGame(Player player);

    Game();
private:
    void createDeck();
};

#endif // GAME_H
